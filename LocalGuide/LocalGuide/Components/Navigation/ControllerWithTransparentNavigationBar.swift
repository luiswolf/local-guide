//
//  ControllerWithTransparentNavigationBar.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit

protocol ControllerWithTransparentNavigationBar: UIViewController {}
