//
//  LocationCollectionViewCell.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import UIKit
import Core

class LocationCollectionViewCell: UICollectionViewCell, Identifiable {
    
    static let spacing: CGFloat = 8.0
    static let nameFont = UIFont.header
    static let typeFont = UIFont.subhead
    static let sizeRange: ClosedRange<CGFloat> = 160...256
    
    private let nameColor = UIColor.topaz
    private let typeColor = UIColor.brownish_grey
    
    private var location: LocationModel?
    private lazy var imageViewHeightConstraint: NSLayoutConstraint = {
        let constraint = imageView.heightAnchor.constraint(equalToConstant: 0)
        constraint.isActive = true
        return constraint
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.backgroundColor = UIColor.Placheholder.home.randomElement()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var roundView: UIView = {
        let view = RoundedView(with: mainStackView)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = Self.spacing
        view.addArrangedSubview(imageView)
        view.addArrangedSubview(captionStackView)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = Self.nameFont
        label.textColor = nameColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.font = Self.typeFont
        label.textColor = typeColor
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var ratingView: RatingView = {
        return RatingView()
    }()
    
    private lazy var captionStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 0.0
        view.addArrangedSubview(nameLabel)
        view.addArrangedSubview(typeLabel)
        view.addArrangedSubview(ratingView)
        view.isLayoutMarginsRelativeArrangement = true
        view.layoutMargins.left = 8.0
        view.layoutMargins.right = 8.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) { nil }

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        guard let attributes = layoutAttributes as? LocationCollectionViewLayoutAttributes else { return }
        imageViewHeightConstraint.constant = attributes.photoHeight
    }
    
}

// MARK: - Setup
extension LocationCollectionViewCell {
    
    private func commonInit() {
        contentView.addSubview(roundView)
        autoLayout()
    }
    
    private func autoLayout() {
        let bottomConstraint = roundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        bottomConstraint.priority = .fittingSizeLevel
        let constraints = [
            roundView.topAnchor.constraint(equalTo: contentView.topAnchor),
            bottomConstraint,
            roundView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            roundView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.widthAnchor.constraint(equalTo: mainStackView.widthAnchor),
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func configure(with location: LocationModel) {
        self.location = location
        nameLabel.text = location.name
        typeLabel.text = location.type
        ratingView.set(rating: location.review)
    }
    
    func set(image: UIImage?) {
        imageView.image = image
    }
    
}
