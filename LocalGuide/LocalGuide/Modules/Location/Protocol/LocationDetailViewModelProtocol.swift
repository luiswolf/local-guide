//
//  LocationDetailViewModelProtocol.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation
import Core

protocol LocationDetailViewModelProtocol {
    
    var delegate: LocationViewModelDelegate? { get set }
    
    init(withProvider provider: NetworkingProviderProtocol, andLocation location: LocationModel)
    
    func getLocation() -> LocationModel
    
    func fetch()
    
}
