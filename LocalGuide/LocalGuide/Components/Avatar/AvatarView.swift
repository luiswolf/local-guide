//
//  AvatarView.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

class AvatarView: UIView {

    private typealias Constant = AvatarConstant
    
    private lazy var outerView: UIView = {
        let view = UIView()
        view.layer.borderWidth = Constant.Size.borderWidth
        view.layer.borderColor = Constant.Color.border.cgColor
        view.layer.cornerRadius = Constant.Size.height / 2
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: Constant.Size.height).isActive = true
        view.widthAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        view.addSubview(backgoundView)
        return view
    }()
    
    private lazy var backgoundView: UIView = {
        let view = UIView()
        view.backgroundColor = Constant.Color.background()
        view.layer.cornerRadius = (Constant.Size.height / 2) - Constant.Size.margin
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var initialsLabel: UILabel = {
        let label = UILabel()
        label.font = Constant.Font.text
        label.textColor = Constant.Color.text
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = Constant.Color.background()
        view.contentMode = .scaleAspectFit
        view.layer.cornerRadius = (Constant.Size.height / 2) - Constant.Size.margin
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Setup
extension AvatarView {
    
    private func commonInit() {
        addSubview(outerView)
        pin(outerView, to: self)
        pin(backgoundView, to: outerView, withMargin: Constant.Size.margin)
    }
    
    private func pin(_ view: UIView, to: UIView, withMargin margin: CGFloat = 0) {
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: to.leadingAnchor, constant: margin),
            view.trailingAnchor.constraint(equalTo: to.trailingAnchor, constant: -margin),
            view.topAnchor.constraint(equalTo: to.topAnchor, constant: margin),
            view.bottomAnchor.constraint(equalTo: to.bottomAnchor, constant: -margin)
        ])
    }
    
    private func center(_ view: UIView, to: UIView) {
        NSLayoutConstraint.activate([
            view.centerYAnchor.constraint(equalTo: to.centerYAnchor),
            view.centerXAnchor.constraint(equalTo: to.centerXAnchor)
        ])
    }
    
    private func clearBackgroundSubviews() {
        backgoundView.subviews.forEach {
            NSLayoutConstraint.deactivate($0.constraints)
            $0.removeFromSuperview()
        }
    }
    
}

// MARK: - Public
extension AvatarView {
    
    func set(image: UIImage?) {
        clearBackgroundSubviews()
        backgoundView.addSubview(imageView)
        pin(imageView, to: backgoundView)
        imageView.image = image
    }
    
    func set(initialsOf name: String) {
        clearBackgroundSubviews()
        backgoundView.addSubview(initialsLabel)
        center(initialsLabel, to: backgoundView)
        initialsLabel.text = name.initials
    }
    
}
