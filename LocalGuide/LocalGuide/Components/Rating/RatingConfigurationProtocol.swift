//
//  RatingConfigurationProtocol.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

protocol RatingConfigurationProtocol {
    
    var isDescriptionEnabled: Bool { get }
    
    var maxRating: Int { get }
    
    var fontSize: CGFloat { get }
    
    var textColor: UIColor { get }
    
    var starSize: CGSize { get }
    
}

// MARK: - Default
extension RatingConfigurationProtocol {
    
    var isDescriptionEnabled: Bool { true }
    
    var maxRating: Int { RatingConstant.maxRating }
    
    var fontSize: CGFloat { RatingConstant.Size.font }
    
    var textColor: UIColor { RatingConstant.Color.text }
    
    var starSize: CGSize { RatingConstant.Size.star }
    
}
