//
//  InformationTableViewCell.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

class InformationTableViewCell: UITableViewCell {

    private typealias Constant = InformationConstant
    
    private lazy var iconImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setContentHuggingPriority(.required, for: .horizontal)
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = Constant.Font.text
        label.textColor = Constant.Color.text
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .top
        view.distribution = .fill
        view.spacing = Constant.Size.spacing
        view.addArrangedSubview(iconImageView)
        view.addArrangedSubview(descriptionLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    required init(with data: InformationDataProtocol) {
        super.init(style: .default, reuseIdentifier: nil)
        commonInit()
        iconImageView.image = data.icon
        descriptionLabel.text = data.description
    }
    
    required init?(coder aDecoder: NSCoder) { nil }

}

// MARK: - Setup
extension InformationTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(mainStackView)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        let constraints = [
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constant.Size.verticalMargin),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constant.Size.verticalMargin),
            mainStackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}
