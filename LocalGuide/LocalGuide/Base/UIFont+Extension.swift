//
//  UIFont+Extension.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import UIKit

extension UIFont {
    
    static let headlineBig = UIFont(name: "OpenSans-Light", size: 32.0)
    static let header = UIFont(name: "OpenSans-Semibold", size: 16.0) ?? .boldSystemFont(ofSize: 16.0)
    
    static let subhead = UIFont(name: "OpenSans", size: 14.0) ?? .systemFont(ofSize: 14.0)
    
    static let caption = UIFont(name: "OpenSans", size: 12.0)
    static let captionBig = UIFont(name: "OpenSans", size: 14.0)
    
    static let textSmall = UIFont(name: "OpenSans-Light", size: 14.0)
    static let text = UIFont(name: "OpenSans-Light", size: 16.0)
    
    static let titleSmall = UIFont(name: "OpenSans", size: 16.0)
    static let title = UIFont(name: "OpenSans", size: 18.0)
    static let titleBig = UIFont(name: "OpenSans", size: 20.0)
    
}
