//
//  RatingConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

struct RatingConstant {
    
    static let maxRating = 5
    
    enum Color {
        static let text = UIColor.black_two
    }
    
    enum Font {
        static let text = UIFont.caption
    }
    
    enum Size {
        static let font: CGFloat = 12.0
        static let descriptionMargin: CGFloat = 3.0
        static let star: CGSize = .init(width: 10.5, height: 10.0)
        static let starSpacing: CGFloat = 8.0
        static let verticalMargin: CGFloat = 4.0
    }
    
}
