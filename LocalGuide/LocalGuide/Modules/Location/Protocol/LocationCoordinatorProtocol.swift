//
//  LocationCoordinatorProtocol.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation

protocol LocationCoordinatorProtocol: class {
    
    func detail(ofLocation location: LocationModel)
    
}
