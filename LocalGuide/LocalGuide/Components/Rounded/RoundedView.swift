//
//  RoundedView.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

class RoundedView: UIView {
    
    private let margin: CGFloat = -12.0
    private let cornerRadius: CGFloat = 16.0
    private let shadowRadius: CGFloat = 1
    private let shadowOpacity: Float = 0.3
    private let shadowOffset: CGSize = .init(width: 0, height: 1)
    
    private var shadowLayer: CAShapeLayer!
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
        return view
    }()
    
    init(with contentView: UIView) {
        super.init(frame: .zero)
        configure(with: contentView)
    }
    
    required init?(coder: NSCoder) { nil }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        shadowLayer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    }
    
}

// MARK: - Setup
extension RoundedView {
    
    private func configure(with subview: UIView) {
        addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        contentView.addSubview(subview)
        NSLayoutConstraint.activate([
            subview.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            subview.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            subview.topAnchor.constraint(equalTo: contentView.topAnchor, constant: margin),
            subview.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: margin)
        ])
        
        guard shadowLayer == nil else { return }
            
        shadowLayer = CAShapeLayer()
        shadowLayer.fillColor = UIColor.clear.cgColor
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowRadius = shadowRadius
        shadowLayer.shadowOffset = shadowOffset
        shadowLayer.shadowOpacity = shadowOpacity
        shadowLayer.shouldRasterize = true
        shadowLayer.rasterizationScale = UIScreen.main.scale
        shadowLayer.cornerRadius = cornerRadius
        shadowLayer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        
        layer.insertSublayer(shadowLayer, at: 0)
    }
    
}
