//
//  LocationWrapperModel.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import Foundation

struct LocationWrapperModel: Codable {
    
    var listLocations: [LocationModel]?
    
}
