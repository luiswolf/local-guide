//
//  LocationViewModelDelegate.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation

protocol LocationViewModelDelegate: class {
    
    func didGetData()
    
    func didGetError(withMessage message: String)
    
}
