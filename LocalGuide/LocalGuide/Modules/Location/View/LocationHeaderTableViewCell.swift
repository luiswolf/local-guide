//
//  LocationHeaderTableViewCell.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

class LocationHeaderTableViewCell: UITableViewCell {
    
    private lazy var coverImageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = UIColor.Placheholder.home.randomElement()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 300.0).isActive = true
        return view
    }()
    
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 0.0
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addArrangedSubview(coverImageView)
        view.addArrangedSubview(dataStackView)
        return view
    }()
    
    private lazy var dataStackView: UIStackView = {
        let view = UIStackView()
        view.addBackground(color: .topaz)
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 10.0
        view.addArrangedSubview(headerLabel)
        view.addArrangedSubview(ratingView)
        view.isLayoutMarginsRelativeArrangement = true
        view.layoutMargins.left = 16.0
        view.layoutMargins.right = 16.0
        view.layoutMargins.top = 16.0
        view.layoutMargins.bottom = 16.0
        return view
    }()
    
    private lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.headlineBig
        label.textColor = .white
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var ratingView: RatingView = {
        return RatingView(withConfiguration: LocationDetailRatingConfiguration())
    }()
    
    init(withLocation location: LocationModel) {
        super.init(style: .default, reuseIdentifier: nil)
        commonInit()
        
        coverImageView.image = UIImage.Placheholder.location.randomElement() as? UIImage
        headerLabel.text = location.name
        ratingView.set(rating: location.review)
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Setup
extension LocationHeaderTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(mainStackView)
        autolayout()
    }
    
    private func autolayout() {
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor)
        ])
    }
    
}
