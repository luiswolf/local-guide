//
//  LocationConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import UIKit

struct LocationConstant {
    
    static let tableName = "LocationLocalizable"
    
    enum Endpoint {
        static let list =
            "https://hotmart-mobile-app.herokuapp.com/locations"
        static let detail = Self.list + "/{id}"
    }
    
    enum Title {
        
        static let about = "Sobre".localized(in: tableName)
        static let home = "Home"
        static let photos = "Fotos".localized(in: tableName)
        static let reviews = "Reviews".localized(in: tableName)
    }
    
    enum Image {
        static let location = UIImage(named: "pin")
        static let phone = UIImage(named: "phone")
        static let share = UIImage(named: "share")
        static let time = UIImage(named: "time")
        static let homeOn = UIImage(named: "home_on")
        static let homeOff = UIImage(named: "home_off")
    }
    
    enum Label {
        static let schedule = "%@: %@"
        static let schedulePeriod = "%@ a %@".localized(in: tableName)
        static let scheduleDescription = "%@ às %@".localized(in: tableName)
        static let seeAllReviews = "ver todos %d reviews".localized(in: tableName)
    }
    
    enum Separator {
        static let and = " e ".localized(in: tableName)
        static let comma = ", "
        static let lineBreak = "\n"
    }
    
    enum Message {
        static let defaultError = "Não foi possível consultar dados. Tente novamente mais tarde.".localized(in: tableName)
        static let noDataError = "Não há dados para exibir.".localized(in: tableName)
    }
    
}
