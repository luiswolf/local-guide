//
//  ReviewModel.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import Foundation

struct ReviewModel: Codable {
    
    let avatarName: String
    let rating: Double
    let title: String
    let content: String
    let author: String
    let location: String
    
}
