//
//  LocationDetailDataProvider.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit
import Core

class LocationDetailDataProvider: NSObject {
    
    private typealias Constant = LocationConstant
    private typealias HeaderFooter = HeaderFooterView
    
    private struct Section {
        var title: String?
        var cells: [UITableViewCell]?
    }
    private var sections = [Section]()
    private var location: LocationModel?
    private let reviews = ReviewViewModel.reviews()
    private let maxReviews = 3
    
    weak var tableView: UITableView?
    
    init(withTableView tableView: UITableView?) {
        self.tableView = tableView
        self.tableView?.backgroundColor = .white
        self.tableView?.separatorStyle = .none
        self.tableView?.allowsSelection = false
        self.tableView?.estimatedRowHeight = UITableView.automaticDimension
        self.tableView?.rowHeight = UITableView.automaticDimension
        if #available(iOS 11.0, *) {
            self.tableView?.contentInsetAdjustmentBehavior = .never
        }
        self.tableView?.register(HeaderFooter.self, forHeaderFooterViewReuseIdentifier: HeaderFooter.identifier)
        super.init()
    }
    
    func set(location: LocationModel) {
        self.location = location
        self.configure()
    }
    
    private func configure() {
        guard let location = location else { return }
        
        sections.removeAll()
        
        // header
        let headerSection = Section(title: nil, cells: [
            LocationHeaderTableViewCell(withLocation: location)
        ])
        sections.append(headerSection)
        
        // photos
        let photos: [UIImage?] = {
            var photos = UIImage.Placheholder.location.filter { $0 != nil }
            photos += photos
            photos.shuffle()
            return photos
        }()
        let photosSection = Section(
            title: Constant.Title.photos,
            cells: [CarouselTableViewCell(with: photos)]
        )
        sections.append(photosSection)
        
        // about
        var aboutCells = [UITableViewCell]()
        if let about = location.about, !about.isEmpty {
            aboutCells.append(BasicTableViewCell(withText: about))
        }
        if let schedule = location.schedule, !schedule.isEmpty {
            let data = LocationScheduleInformationData(with: schedule)
            aboutCells.append(InformationTableViewCell(with: data))
        }
        if let phone = location.phone, !phone.isEmpty {
            let data = LocationPhoneInformationData(with: phone)
            aboutCells.append(InformationTableViewCell(with: data))
        }
        if let address = location.address, !address.isEmpty {
            let data = LocationAddressInformationData(with: address)
            aboutCells.append(InformationTableViewCell(with: data))
        }
        let aboutSection = Section(
            title: Constant.Title.about,
            cells: aboutCells
        )
        
        if !aboutCells.isEmpty {
            sections.append(aboutSection)
        }
        
        // reviews
        if let reviews = reviews, !reviews.isEmpty {
            var reviewsCells = [UITableViewCell]()
            for item in reviews.prefix(maxReviews) {
                reviewsCells.append(ReviewTableViewCell(withData: item))
            }
            if reviews.count > maxReviews {
                reviewsCells.append(
                    ReviewDetailTableViewCell(
                        withText: String(
                            format: Constant.Label.seeAllReviews,
                            arguments: [reviews.count]
                        )
                    )
                )
            }
            sections.append(Section(title: Constant.Title.reviews, cells: reviewsCells))
        }
    }
    
}

// MARK: - UITableViewDelegate
extension LocationDetailDataProvider: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].cells?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section].cells?[indexPath.row] ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard sections[section].title != nil else {
            return .leastNonzeroMagnitude
        }
        return UITableView.automaticDimension
    }
    
}

// MARK: - UITableViewDataSource
extension LocationDetailDataProvider: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let title = sections[section].title else {
            return nil
        }
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderFooter.identifier) as? HeaderFooter
        header?.set(title: title)
        return header
    }
    
}
