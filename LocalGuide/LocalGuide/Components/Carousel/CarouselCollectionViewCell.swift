//
//  CarouselCollectionViewCell.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit
import Core

class CarouselCollectionViewCell: UICollectionViewCell, Identifiable {
    
    private typealias Constant = CarouselConstant
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = Constant.Size.cornerRadius
        view.layer.masksToBounds = true
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) { nil }
    
}

// MARK: - Setup
extension CarouselCollectionViewCell {
    
    private func commonInit() {
        contentView.addSubview(imageView)
        autoLayout()
    }
    
    private func autoLayout() {
        let constraints = [
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}

// MARK: - Public
extension CarouselCollectionViewCell {
    
    func set(image: UIImage?) {
        imageView.image = image
    }
    
}
