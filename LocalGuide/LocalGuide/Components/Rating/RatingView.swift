//
//  RatingView.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import UIKit

class RatingView: UIView {

    private typealias Constant = RatingConstant
    
    private var configuration: RatingConfigurationProtocol = RatingDefaultConfiguration()
    
    private lazy var ratingStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = Constant.Size.descriptionMargin
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = Constant.Color.text
        label.font = Constant.Font.text
        return label
    }()
    
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .center
        view.distribution = .fill
        view.spacing = Constant.Size.starSpacing
        view.addArrangedSubview(ratingStackView)
        view.addArrangedSubview(descriptionLabel)
        view.isLayoutMarginsRelativeArrangement = true
        view.layoutMargins.top = Constant.Size.verticalMargin
        view.layoutMargins.bottom = Constant.Size.verticalMargin
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func set(rating value: Double) {
        let roundedValue = round(value)
        ratingStackView.arrangedSubviews.forEach {
            ratingStackView.removeArrangedSubview($0)
            NSLayoutConstraint.deactivate($0.constraints)
            $0.removeFromSuperview()
        }
        for i in 1...configuration.maxRating {
            let isOn = roundedValue >= Double(i)
            ratingStackView.addArrangedSubview(
                StarImageView(withSize: configuration.starSize, isOn: isOn)
            )
        }
        descriptionLabel.text = String(value)
    }
    
    init(withConfiguration configuration: RatingConfigurationProtocol) {
        self.configuration = configuration
        super.init(frame: .zero)
        descriptionLabel.isHidden = !configuration.isDescriptionEnabled
        descriptionLabel.font = descriptionLabel.font.withSize(configuration.fontSize)
        descriptionLabel.textColor = configuration.textColor
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Setup
extension RatingView {
    
    private func commonInit() {
        addSubview(mainStackView)
        autoLayout()
    }
    
    private func autoLayout() {
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: topAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
}
