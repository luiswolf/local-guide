//
//  LocationDataProviderDelegate.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import Foundation

protocol LocationDataProviderDelegate: class {
    
    func didSelect(item: LocationModel)
    
}
