//
//  StarConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation

struct StarConstant {
    
    enum Image {
        static let on = "on"
        static let off = "off"
    }
    
}
