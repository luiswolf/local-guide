//
//  ReviewConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit

struct ReviewConstant {
    
    enum Image {
        static let detail = UIImage(named: "arrows")
    }
    
    enum Label {
        static let caption = "%@, %@"
    }

}
