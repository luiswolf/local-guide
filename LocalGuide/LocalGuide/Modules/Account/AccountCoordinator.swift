//
//  AccountCoordinator.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit
import Core

class AccountCoordinator: CoordinatorProtocol {
    
    var childCoordinators = [CoordinatorProtocol]()
    
    var navigationController: UINavigationController
    
    private lazy var vc: UIViewController = {
        let vc = AccountViewController()
        vc.tabBarItem = UITabBarItem(
            title: String(),
            image: AccountConstant.Image.profileOff,
            selectedImage: AccountConstant.Image.profileOn
        )
        vc.navigationItem.title = AccountConstant.Title.profile
        return vc
    }()

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        navigationController.pushViewController(vc, animated: true)
    }
    
}
