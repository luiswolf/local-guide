//
//  LocationDetailTableViewController.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit
import Core

class LocationDetailTableViewController: UITableViewController, LoaderProtocol, ErrorProtocol, ErrorViewDelegate, ControllerWithTransparentNavigationBar {

    private typealias Constant = LocationConstant
    
    internal var errorDelegate: ErrorViewDelegate? { self }
    
    private var viewModel: LocationDetailViewModelProtocol
    
    lazy var navigation: NavigationController? = {
        return navigationController as? NavigationController
    }()

    private lazy var dataProvider: LocationDetailDataProvider = {
        return LocationDetailDataProvider(withTableView: tableView)
    }()
    
    private lazy var shareButton: UIBarButtonItem = {
        return UIBarButtonItem(
            image: Constant.Image.share,
            style: .plain,
            target: self, action:
            #selector(share)
        )
    }()

    init(withViewModel viewModel: LocationDetailViewModelProtocol) {
        self.viewModel = viewModel
        super.init(style: .grouped)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Lifecycle
extension LocationDetailTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = dataProvider
        tableView.dataSource = dataProvider
        navigationItem.rightBarButtonItem = shareButton
        fetchData()
        navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnSwipe = false
    }
    
    override func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
}

// MARK: - Helper
extension LocationDetailTableViewController {
    
    private func fetchData() {
        hideError()
        showLoader()
        viewModel.fetch()
    }
    
}

// MARK: - Actions
extension LocationDetailTableViewController {
    
    @objc
    private func share() {}
    
}

// MARK: - LocationViewModelDelegate
extension LocationDetailTableViewController: LocationViewModelDelegate {
    
    func didGetData() {
        hideLoader()
        dataProvider.set(location: viewModel.getLocation())
        tableView.reloadData()
    }
    
    func didGetError(withMessage message: String) {
        navigation?.setFillNavigationBar()
        hideLoader()
        showError(withMessage: message)
    }
    
    func didTryAgain() {
        navigation?.setClearNavigationBar()
        fetchData()
    }
    
}
