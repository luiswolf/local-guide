//
//  LocationDataProvider.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import UIKit
import Core
import AVFoundation

class LocationDataProvider: NSObject {
    
    typealias Delegate = LocationDataProviderDelegate
    typealias ItemModel = LocationModel
    private typealias ItemCell = LocationCollectionViewCell
    
    weak var delegate: Delegate?
    weak var collectionView: UICollectionView?
    
    private var items: [ItemModel]?
    private lazy var photos: [UIImage?]? = {
        guard let count = items?.count else { return nil }
        return (0..<count)
            .map { _ in UIImage.Placheholder.location.randomElement() as? UIImage }
    }()
    
    func set(items: [ItemModel]?) {
        self.items = items
    }
    
    init(withCollectionView collectionView: UICollectionView?) {
        super.init()
        self.collectionView = collectionView
        self.collectionView?.backgroundColor = .white
        self.collectionView?.register(ItemCell.self, forCellWithReuseIdentifier: ItemCell.identifier)
    }
    
}

// MARK: - UICollectionViewDelegate
extension LocationDataProvider: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let items = items else { return }
        delegate?.didSelect(item: items[indexPath.item])
    }
    
}

// MARK: - UICollectionViewDataSource
extension LocationDataProvider: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let items = items else { return UICollectionViewCell() }
        return cell(forItem: items[indexPath.item], atIndexPath: indexPath)
    }
    
}

// MARK: - Helper
extension LocationDataProvider {
    
    private func dequeue<T: Identifiable>(cellOfType type: T.Type, atIndexPath indexPath: IndexPath) -> T {
        return collectionView?.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as! T
    }
    
    private func cell(forItem item: ItemModel,  atIndexPath indexPath: IndexPath) -> ItemCell {
        let cell = dequeue(cellOfType: ItemCell.self, atIndexPath: indexPath)
        cell.configure(with: item)
        cell.set(image: photos?[indexPath.item])
        return cell
    }

}

// MARK: - LocationCollectionViewLayoutDelegate
extension LocationDataProvider: LocationCollectionViewLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath, withWidth width: CGFloat) -> CGFloat {
        guard let photo = photos?[indexPath.item] else { return CGFloat.random(in: ItemCell.sizeRange) }
        let boundingRect =  CGRect(x: 0, y: 0, width: width, height: CGFloat(MAXFLOAT))
        let rect  = AVMakeRect(aspectRatio: photo.size, insideRect: boundingRect)
        return rect.size.height
    }
    
    func collectionView(_ collectionView: UICollectionView, heightForCaptionAtIndexPath indexPath: IndexPath, withWidth width: CGFloat) -> CGFloat {
        guard let items = items else { return 0.0 }
        
        let item = items[indexPath.item]
        
        var height: CGFloat = 0.0
        height += ItemCell.spacing // margin top
        height += item.name.height(with: ItemCell.nameFont, width: width) // name
        height += item.type.height(with: ItemCell.typeFont, width: width) // type
        height += ItemCell.spacing // margin bottom
        return height
    }
    
}
