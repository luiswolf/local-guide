//
//  String+Extension.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

extension String {
    
    var initials: String {
        return split(separator: " ")
            .map{ String($0.first ?? Character(String()))}
            .prefix(2)
            .joined()
    }
        
    func height(with font: UIFont, width: CGFloat) -> CGFloat {
        let nsstring = NSString(string: self)
        let maxHeight = CGFloat(64.0)
        let textAttributes = [NSAttributedString.Key.font: font]
        
        let boundingRect = nsstring.boundingRect(with: CGSize(width: width, height: maxHeight), options: .usesLineFragmentOrigin, attributes: textAttributes, context: nil)
        return ceil(boundingRect.height)
    }
    
    func localized(in tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, bundle: .main, value: "**\(self)**", comment: String())
    }
    
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\+\\d{2})?(\\d{2})(\\d{1})?(\\d{4})(\\d{4})", with: "$1 $2 $3$4 $5", options: .regularExpression, range: nil)
    }
        
}
