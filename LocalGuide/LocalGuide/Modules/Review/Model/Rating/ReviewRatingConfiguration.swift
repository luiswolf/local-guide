//
//  ReviewRatingConfiguration.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation

struct ReviewRatingConfiguration: RatingConfigurationProtocol {
    
    var isDescriptionEnabled: Bool { false }
    
}
