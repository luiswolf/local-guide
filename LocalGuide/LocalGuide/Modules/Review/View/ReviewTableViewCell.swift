//
//  ReviewTableViewCell.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    private typealias Constant = ReviewConstant
    
    private let mainSpacing: CGFloat = 16.0
    private let dataSpacing: CGFloat = 4.0
    private let verticalMargin: CGFloat = 8.0
    private let titleFont = UIFont.titleSmall
    private let titleColor = UIColor.topaz
    private let contentColor = UIColor.brownish_grey
    private let contentFont = UIFont.textSmall
    private let captionColor = UIColor.warm_grey
    private let captionFont = UIFont.caption
    
    private lazy var mainStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .top
        view.distribution = .fill
        view.spacing = mainSpacing
        view.addArrangedSubview(avatarView)
        view.addArrangedSubview(dataStackView)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var avatarView: AvatarView = {
        return AvatarView()
    }()
    
    private lazy var dataStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .leading
        view.distribution = .fill
        view.spacing = dataSpacing
        
        view.addArrangedSubview(ratingView)
        view.addArrangedSubview(titleLabel)
        view.addArrangedSubview(contentLabel)
        view.addArrangedSubview(captionLabel)
        return view
    }()
    
    private lazy var ratingView: RatingView = {
        return RatingView(withConfiguration: ReviewRatingConfiguration())
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = titleColor
        label.font = titleFont
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.textColor = contentColor
        label.font = contentFont
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var captionLabel: UILabel = {
        let label = UILabel()
        label.textColor = captionColor
        label.font = captionFont
        label.numberOfLines = 0
        return label
    }()
    
    init(withData data: ReviewModel) {
        super.init(style: .default, reuseIdentifier: nil)
        commonInit()
        configure(with: data)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Setup
extension ReviewTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(mainStackView)
        autolayout()
    }
    
    private func autolayout() {
        let margin = contentView.layoutMarginsGuide
        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: margin.topAnchor, constant: verticalMargin),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -verticalMargin),
            mainStackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ])
    }
    
}

// MARK: - Public
extension ReviewTableViewCell {

    func configure(with review: ReviewModel) {
        if let image = UIImage(named: review.avatarName) {
            avatarView.set(image: image)
        } else {
            avatarView.set(initialsOf: review.author)
        }
        ratingView.set(rating: review.rating)
        titleLabel.text = review.title
        contentLabel.text = review.content
        captionLabel.text = String(format: Constant.Label.caption, arguments: [review.author, review.location])
    }

}
