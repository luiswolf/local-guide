//
//  LocationDetailRequest.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import Foundation
import Core

struct LocationDetailRequest: NetworkingRequestProtocol {
    
    var id: LocationModel.Id
    
    var method: NetworkingMethod { .get }
    
    var url: String {
        return LocationConstant.Endpoint.detail
            .replacingOccurrences(of: "{id}", with: String(id))
    }
    
    init(with id: LocationModel.Id) {
        self.id = id
    }
    
}
