//
//  InformationConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

struct InformationConstant {
    
    enum Color {
        static let text = UIColor.black
    }
    
    enum Font {
        static let text = UIFont.textSmall
    }
    
    enum Size {
        static let verticalMargin: CGFloat = 8.0
        static let spacing: CGFloat = 8.0
    }
    
}
