//
//  InformationScheduleData.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

struct LocationScheduleInformationData: InformationDataProtocol {
    
    private typealias Constant = LocationConstant
    
    var icon: UIImage? { Constant.Image.time }
    
    var description: String
    
    init(with schedules: LocationModel.Schedules) {
        let formatter = ScheduleFormatter(withItems: schedules.map { $0 })
        description = formatter.description() ?? String()
    }
    
}
