//
//  ReviewDetailTableViewCell.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

class ReviewDetailTableViewCell: UITableViewCell {

    private typealias Constant = ReviewConstant
    
    private let labelFont = UIFont.captionBig
    private let labelColor = UIColor.topaz
    
    public convenience init(withText text: String) {
        self.init()
        textLabel?.text = text
        textLabel?.numberOfLines = 0
        textLabel?.font = labelFont
        textLabel?.textColor = labelColor
        textLabel?.textAlignment = .right
        accessoryView = UIImageView(image: Constant.Image.detail)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) { nil }
    
}
