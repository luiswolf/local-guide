//
//  MapCoordinator.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit
import Core

class MapCoordinator: CoordinatorProtocol {
    
    var childCoordinators = [CoordinatorProtocol]()
    
    var navigationController: UINavigationController
    
    private lazy var vc: UIViewController = {
        let vc = MapViewViewController()
        vc.tabBarItem = UITabBarItem(
            title: String(),
            image: MapConstant.Image.mapOff,
            selectedImage: MapConstant.Image.mapOn
        )
        vc.navigationItem.title = MapConstant.Title.map
        return vc
    }()

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        navigationController.pushViewController(vc, animated: true)
    }
    
}

