//
//  LocationPhoneInformationData.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

struct LocationPhoneInformationData: InformationDataProtocol {
    
    private typealias Constant = LocationConstant
    
    var icon: UIImage? { Constant.Image.phone }
    
    var description: String
    
    init(with phone: String) {
        description = phone.toPhoneNumber()
    }
    
}
