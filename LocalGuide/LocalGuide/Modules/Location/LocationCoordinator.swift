//
//  LocationCoordinator.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import UIKit
import Core

class LocationCoordinator: CoordinatorProtocol, LocationCoordinatorProtocol {
    
    var childCoordinators = [CoordinatorProtocol]()
    
    var navigationController: UINavigationController
    
    private let provider = NetworkingProvider()
    
    private lazy var vc: UICollectionViewController = {
        let vm = LocationViewModel(withProvider: provider)
        let vc = LocationCollectionViewController(withViewModel: vm)
        vc.tabBarItem = UITabBarItem(
            title: String(),
            image: LocationConstant.Image.homeOff,
            selectedImage: LocationConstant.Image.homeOn
        )
        vc.navigationItem.title = LocationConstant.Title.home
        vc.navigationItem.backBarButtonItem = UIBarButtonItem(title: String(), style: .plain, target: nil, action: nil)
        vc.coordinator = self
        return vc
    }()

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        navigationController.pushViewController(vc, animated: true)
    }
    
}

// MARK: - Actions
extension LocationCoordinator {
    
    func detail(ofLocation location: LocationModel) {
        let vm = LocationDetailViewModel(withProvider: provider, andLocation: location)
        let vc = LocationDetailTableViewController(withViewModel: vm)
        vc.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(vc, animated: true)
    }
    
}
