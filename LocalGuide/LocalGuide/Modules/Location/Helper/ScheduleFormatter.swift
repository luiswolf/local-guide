//
//  ScheduleFormatter.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 27/03/21.
//

import Foundation

class ScheduleFormatter {
    
    private let helper = DateHelper.shared
    private typealias Constant = LocationConstant
    
    init(withItems items: [LocationModel.Schedules.Element]) {
        let sorted = items.sorted {
            guard
                let lhs = helper.weekday(from: $0.key),
                let rhs = helper.weekday(from: $1.key)
            else { return false }
            return lhs < rhs
        }
        sorted.forEach {
            add(item: $0)
        }
    }
    
    var items: [Schedule]?
    
    class Schedule {
        private let helper = DateHelper.shared
        
        var description: String
        var days: [Date]
        
        func add(day: Date) {
            days.append(day)
        }
        
        func isSequentialDays() -> Bool {
            var isSequential: Bool = true
            for currentIndex in 0..<days.count {
                let nextIndex = currentIndex + 1
                guard days.indices.contains(nextIndex) else { break }
                let currentDate = helper.weekday(from: days[currentIndex])
                let nextDate = helper.weekday(from: days[nextIndex])
                if (nextDate - currentDate) > 1 {
                    isSequential = false
                }
            }
            return isSequential
        }
        
        init(description: String, days: [Date]) {
            self.description = description
            self.days = days
        }
    }
    
    private func add(item: LocationModel.Schedules.Element) {
        // convert string key to date
        guard let date = helper.date(from: item.key) else { return }
        
        items = items ?? []
        
        // 8h as 18h
        let description = String(
            format: Constant.Label.scheduleDescription,
            arguments: [
                item.value.open,
                item.value.close
            ]
        )
        guard let existent = items?.first(where: { $0.description == description }) else {
            items?.append(Schedule(description: description, days: [date]))
            return
        }
        existent.add(day: date)
    }
    
    func description() -> String? {
        var elements: [String] = []
        items?.forEach({ item in
            let stringDays = item.days.map { helper.dayName(from: $0) }
            let groupCondition = item.days.count > 2
            
            
            let x: String = {
                guard item.isSequentialDays() else {
                    return stringDays.joined(separator: Constant.Separator.comma)
                }
                guard groupCondition, let first = item.days.first, let last = item.days.last else {
                    return stringDays.joined(separator: Constant.Separator.and)
                }
                return String(
                    format: Constant.Label.schedulePeriod,
                    arguments: [
                        helper.dayName(from: first),
                        helper.dayName(from: last)
                    ]
                )
            }()
            let schedule = String(
                format: Constant.Label.schedule,
                arguments: [
                    x,
                    item.description
                ]
            )
            elements.append(schedule)
        })
        return elements.joined(separator: Constant.Separator.lineBreak)
    }

}
