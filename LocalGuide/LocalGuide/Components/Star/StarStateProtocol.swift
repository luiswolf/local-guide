//
//  StarStateProtocol.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

protocol StarStateProtocol {
    
    var image: UIImage? { get }
    
}
