//
//  InformationDataProtocol.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

protocol InformationDataProtocol {
    
    var icon: UIImage? { get }
    
    var description: String { get }
    
}
