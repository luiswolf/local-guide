//
//  StarImageView.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

class StarImageView: UIImageView {
    
    private let size: CGSize
    
    init(withSize size: CGSize, isOn: Bool) {
        self.size = size
        super.init(frame: .zero)
        let state: StarStateProtocol = isOn ? StarOnState() : StarOffState()
        configure(with: state)
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Setup
extension StarImageView {

    private func configure(with state: StarStateProtocol) {
        image = state.image
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: size.width),
            heightAnchor.constraint(equalToConstant: size.height),
        ])
    }

}
