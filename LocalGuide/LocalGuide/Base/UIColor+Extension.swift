//
//  UIColor+Extension.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import UIKit

extension UIColor {
    
    static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    static let black = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    static let black_20 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2)
    static let black_25 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.25)
    static let black_two = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    
    static let brownish_grey = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
    static let pinkish_grey = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
    static let warm_grey = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
    
    static let creme = #colorLiteral(red: 0.9882352941, green: 1, blue: 0.6941176471, alpha: 1)
    static let golden_yellow = #colorLiteral(red: 1, green: 0.7803921569, blue: 0.1176470588, alpha: 1)
    
    static let topaz = #colorLiteral(red: 0.07843137255, green: 0.6784313725, blue: 0.7215686275, alpha: 1)
    static let blue = #colorLiteral(red: 0.07450980392, green: 0.8078431373, blue: 0.8549019608, alpha: 1)
    static let aqua_green = #colorLiteral(red: 0.0862745098, green: 0.8980392157, blue: 0.6392156863, alpha: 1)
    static let duck_egg_blue = #colorLiteral(red: 0.8156862745, green: 0.9921568627, blue: 0.937254902, alpha: 1)
    
    static let pink = #colorLiteral(red: 1, green: 0.3450980392, blue: 0.462745098, alpha: 1)
    static let light_pink = #colorLiteral(red: 1, green: 0.8549019608, blue: 0.8823529412, alpha: 1)
    static let light_pink_alternative = #colorLiteral(red: 1, green: 0.8549019608, blue: 0.8784313725, alpha: 1)
    
    enum Placheholder {
        static let avatar: [UIColor] = [.aqua_green, .blue, .pink]
        static let home: [UIColor] = [.duck_egg_blue, .light_pink_alternative, .creme]
    }
    
}
