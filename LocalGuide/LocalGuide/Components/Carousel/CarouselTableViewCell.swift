//
//  CarouselTableViewCell.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit
import Core

class CarouselTableViewCell: UITableViewCell {

    private typealias Cell = CarouselCollectionViewCell
    private typealias Constant = CarouselConstant
    
    private let images: [UIImage?]
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.dataSource = self
        view.delegate = self
        view.showsHorizontalScrollIndicator = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.heightAnchor.constraint(equalToConstant: Constant.Size.height).isActive = true
        return view
    }()
    
    required init(with images: [UIImage?]) {
        self.images = images
        super.init(style: .default, reuseIdentifier: nil)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { nil }
}

// MARK: - Setup
extension CarouselTableViewCell {
    
    private func commonInit() {
        collectionView.register(Cell.self, forCellWithReuseIdentifier: Cell.identifier)
        contentView.addSubview(collectionView)
        selectionStyle = .none
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView
        let bottom = collectionView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        bottom.priority = .fittingSizeLevel
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: margin.topAnchor),
            bottom,
            collectionView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ])
    }
    
}

// MARK: - UICollectionViewDataSource
extension CarouselTableViewCell: UICollectionViewDataSource {
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.identifier, for: indexPath) as? Cell else {
            return UICollectionViewCell()
        }
        cell.set(image: images[indexPath.item])
        return cell
    }

}

// MARK: - UICollectionViewDelegateFlowLayout
extension CarouselTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return Constant.Size.spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return Constant.Size.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constant.Size.itemSpacing
    }
    
}
