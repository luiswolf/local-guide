//
//  LocationModel.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import Foundation

struct LocationModel: Codable {
    
    typealias Id = Int
    typealias Schedules = [String: Schedule]
    
    struct Schedule: Codable {
        let open: String
        let close: String
    }
    
    let id: Id
    let name: String
    let review: Double
    let type: String
    
    // optionals
    let about: String?
    let schedule: Schedules?
    let phone: String?
    let address: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, review, type, about, schedule, phone, address
    }
        
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Id.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        review = try values.decode(Double.self, forKey: .review)
        type = try values.decode(String.self, forKey: .type)
        // optionals
        about = try? values.decode(String.self, forKey: .about)
        schedule = try? values.decode(Schedules.self, forKey: .schedule)
        phone = try? values.decode(String.self, forKey: .phone)
        address = try? values.decode(String.self, forKey: .address)
    }
    
}
