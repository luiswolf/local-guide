//
//  AppDelegate.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import UIKit
import Core

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.title!
        ]
        UITabBar.appearance().backgroundColor = .white
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().unselectedItemTintColor = .topaz
        UITabBar.appearance().tintColor = .topaz
        
        UITabBar.appearance().backgroundImage = UIImage.colorForNavBar(color: .white)
        UITabBar.appearance().shadowImage = UIImage.colorForNavBar(color: .topaz)
        
        BasicTableViewCell.appearance().customFont = UIFont.text
        BasicTableViewCell.appearance().customTextColor = .black

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = TabBarController()
        window.makeKeyAndVisible()
        window.tintColor = .black
        
        self.window = window
        return true
    }
    

}
