//
//  LocationDetailRatingConfiguration.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

struct LocationDetailRatingConfiguration: RatingConfigurationProtocol {
    
    var fontSize: CGFloat { 14.0 }
    
    var textColor: UIColor { .white }
    
    var starSize: CGSize { CGSize(width: 16.0, height: 15.0) }
    
}
