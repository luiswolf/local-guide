//
//  AccountConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit

struct AccountConstant {
    
    static let tableName = "AccountLocalizable"
    
    enum Image {
        static let profileOn = UIImage(named: "profile_on")
        static let profileOff = UIImage(named: "profile_off")
    }
    
    enum Title {
        static let profile = "Perfil".localized(in: tableName)
    }
    
}
