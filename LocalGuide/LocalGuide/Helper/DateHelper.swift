//
//  DateHelper.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 27/03/21.
//

import Foundation

class DateHelper {
    
    private let formatter = DateFormatter()
    static let shared = DateHelper()
    
    private init() {}
    
    func date(from dayOfWeek: String) -> Date? {
        formatter.locale = Locale(identifier: "en_us")
        formatter.dateFormat = "EEEE"
        return formatter.date(from: dayOfWeek)
    }
    
    func dayName(from date: Date) -> String {
        formatter.locale = Locale.current
        formatter.dateFormat = "E"
        return String(formatter.string(from: date).lowercased().prefix(3))
    }
    
    func weekday(from dayOfWeek: String) -> Int? {
        guard let date = date(from: dayOfWeek) else { return nil }
        return weekday(from: date)
    }
    
    func weekday(from date: Date) -> Int {
        let components = Calendar.current.dateComponents([.weekday], from: date)
        return components.weekday!
    }
    
}
