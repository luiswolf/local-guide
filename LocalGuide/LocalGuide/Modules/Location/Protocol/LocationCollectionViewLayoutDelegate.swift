//
//  LocationCollectionViewLayoutDelegate.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit

protocol LocationCollectionViewLayoutDelegate: AnyObject {
     
    func collectionView(_ collectionView:UICollectionView,
                        heightForPhotoAtIndexPath indexPath:IndexPath,
                        withWidth width: CGFloat) -> CGFloat
    
    func collectionView(_ collectionView: UICollectionView,
                        heightForCaptionAtIndexPath indexPath: IndexPath,
                        withWidth width: CGFloat) -> CGFloat
    
}
