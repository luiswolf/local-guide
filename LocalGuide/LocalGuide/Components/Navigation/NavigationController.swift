//
//  NavigationController.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

class NavigationController: UINavigationController {
    
    private typealias Constant = NavigationConstant
    
    init() {
        super.init(nibName: nil, bundle: nil)
        delegate = self
        navigationBar.backIndicatorImage = Constant.Image.backButton
        navigationBar.backIndicatorTransitionMaskImage = Constant.Image.backButton
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

// MARK: - UINavigationControllerDelegate
extension NavigationController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        configure(forViewController: viewController)
    }
    
}

// MARK: - Helper
extension NavigationController {
    
    private func configure(forViewController viewController: UIViewController) {
        guard viewController is ControllerWithTransparentNavigationBar else {
            setFillNavigationBar()
            return
        }
        setClearNavigationBar()
    }
    
    func setFillNavigationBar() {
        navigationBar.setBackgroundImage(nil, for: .default)
        navigationBar.barTintColor = Constant.Color.main
        navigationBar.backgroundColor = Constant.Color.main
        navigationBar.shadowImage = nil
        navigationBar.isTranslucent = false
        view.backgroundColor = Constant.Color.main
    }
    
    func setClearNavigationBar() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.barTintColor = .clear
        navigationBar.backgroundColor = .clear
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = .clear
    }
    
}
