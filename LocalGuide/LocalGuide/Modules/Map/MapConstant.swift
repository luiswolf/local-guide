//
//  MapConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit

struct MapConstant {

    static let tableName = "MapLocalizable"
    
    enum Image {
        static let mapOn = UIImage(named: "map_on")
        static let mapOff = UIImage(named: "map_off")
    }
    
    enum Title {
        static let map = "Mapa".localized(in: tableName)
    }

}
