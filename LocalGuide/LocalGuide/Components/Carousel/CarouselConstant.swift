//
//  CarouselConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

struct CarouselConstant {
    
    enum Size {
        static let cornerRadius: CGFloat = 8.0
        static let spacing: UIEdgeInsets = .init(top: 16, left: 20, bottom: 16, right: 20)
        static let itemSize: CGSize = .init(width: 60, height: 60)
        static let itemSpacing: CGFloat = 16.0
        static let height: CGFloat = 92.0
    }
    
}
