# Local Guide

Aplicativo desenvolvido em teste para vaga de desenvolvedor iOS entre os dias 22/03/2021 e 27/03/2021.

## Requisitos

* iOS 10.0+
* Xcode 12.4+
* Swift 5.3.2+

## Considerações

* Construção de layouts com ViewCoding
* Modelo arquitetural com o padrão MVVM
* Versão mínima do iOS 10 conforme especificação do teste
* Navegação entre telas com o padrão Coordinator para retirar esta responsabilidade dos ViewControllers
* Separação de funcionalidades por módulos, apesar de ser uma aplicação pequena
* Comunicação entre objetos a partir de delegates
* Aplicação de conceitos do SOLID e Programação orientada a protocolos
* Internacionalização

## Dependências

Para gerenciamento de dependências, utilizei o Swift Package Manager com as bibliotecas abaixo:

* [Alamofire] - Biblioteca que realiza chamadas de serviços para APIs Rest.
* [Core] - Biblioteca pessoal criada para facilitar o desenvolvimento de testes pois contém implementações comuns em qualquer tipo de aplicação.

[Alamofire]: <https://github.com/Alamofire/Alamofire>
[Core]: <https://gitlab.com/luiswolf/ios-core.git>

## Apresentação da aplicação

### Home

Tela principal da aplicação, onde são apresentados os locais consultados a partir da API.
As imagens apresentadas estão disponíveis no próprio projeto e são carregadas aleatóriamente. Caso a imagem não exista, é exibido apenas o background nas cores especificadas no teste.
    
<br>  
<img src="screenshots/screenshot-05.png" width="300"><img src="screenshots/screenshot-06.png" width="300">

### Detalhe do local

Apresentação dos dados do local:
* obrigatórios: nome, categoria, avaliação
* opcionais (caso retornado pela API): sobre, horário de funcionamento, telefone.

Todas imagens e reviews são carregadas localmente (do próprio projeto) e aleatóriamente.
    
<br>  
<img src="screenshots/screenshot-07.png" width="300"><img src="screenshots/screenshot-08.png" width="300">
   
### Reviews

As reviews do local são carregadas aleatóriamente e estão armazendas em um arquivo JSON localmente, já que a API não disponibiliza estes dados. 
    
<br>   
<img src="screenshots/screenshot-03.png" width="300"><img src="screenshots/screenshot-04.png" width="300">

### Tratamento de erros

Tanto na tela principal quanto na tela de detalhe do local, caso ocorra um problema de API ou a mesma não retorne dados, é exibida tela de erro com possibilidade de realizar a requisição novamente.
    
<br>   
<img src="screenshots/screenshot-09.png" width="300"><img src="screenshots/screenshot-10.png" width="300">

### Internacionalização

Conforme especificação, foi adicionado internacionalização no projeto com os idiomas Ingês e Português.
    
<br>   
<img src="screenshots/screenshot-01.png" width="300"><img src="screenshots/screenshot-02.png" width="300">
