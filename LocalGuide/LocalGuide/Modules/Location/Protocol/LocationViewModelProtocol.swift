//
//  LocationViewModelProtocol.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation
import Core

protocol LocationViewModelProtocol {
    
    var delegate: LocationViewModelDelegate? { get set }
    
    init(withProvider provider: NetworkingProviderProtocol)
    
    func getLocations() -> [LocationModel]?
    
    func fetch()
    
}
