//
//  TabBarController.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import UIKit

class TabBarController: UITabBarController {

    let location = LocationCoordinator(navigationController: NavigationController())
    let map = MapCoordinator(navigationController: NavigationController())
    let account = AccountCoordinator(navigationController: NavigationController())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        location.start()
        map.start()
        account.start()
        viewControllers = [
            location.navigationController,
            map.navigationController,
            account.navigationController
        ]
    }

}
