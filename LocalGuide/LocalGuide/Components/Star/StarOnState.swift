//
//  StarOnState.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

struct StarOnState: StarStateProtocol {
    
    private typealias Constant = StarConstant.Image
    
    var image: UIImage? { UIImage(named: Constant.on) }
    
}
