//
//  LocationDetailViewModel.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import Foundation
import Core

class LocationDetailViewModel: NSObject, LocationDetailViewModelProtocol {
    
    private typealias Constant = LocationConstant
    
    weak var delegate: LocationViewModelDelegate?
    
    private var location: LocationModel
    
    private let neworkingProvider: NetworkingProviderProtocol
    
    private lazy var requestDetail: LocationDetailRequest = {
        return LocationDetailRequest(with: location.id)
    }()
    
    required init(withProvider provider: NetworkingProviderProtocol = NetworkingProvider(), andLocation location: LocationModel) {
        self.neworkingProvider = provider
        self.location = location
    }
    
    func getLocation() -> LocationModel { location }
    
}

// MARK: - Networking
extension LocationDetailViewModel {
    
    func fetch() {
        neworkingProvider.perform(requestDetail) { [weak self] (response: LocationModel?) in
            guard let response = response else {
                self?.delegate?.didGetError(withMessage: Constant.Message.defaultError)
                return
            }
            self?.location = response
            self?.delegate?.didGetData()
        }
    }
    
}
