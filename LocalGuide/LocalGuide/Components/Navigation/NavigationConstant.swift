//
//  NavigationConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

struct NavigationConstant {
    
    enum Color {
        static let main = UIColor.topaz
    }
    
    enum Image {
        static let backButton = UIImage(named: "arrow_back")
    }
    
}
