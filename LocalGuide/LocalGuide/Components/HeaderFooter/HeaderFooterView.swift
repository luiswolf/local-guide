//
//  HeaderFooterView.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit
import Core

class HeaderFooterView: UITableViewHeaderFooterView, Identifiable {
    
    private typealias Constant = HeaderFooterConstant
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = Constant.Font.text
        label.textColor = Constant.Color.text
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Setup
extension HeaderFooterView {
    
    private func commonInit() {
        contentView.addSubview(titleLabel)
        autolayout()
    }
    
    private func autolayout() {
        let margin = contentView.layoutMarginsGuide
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constant.Size.topMargin),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
}

// MARK: - Public
extension HeaderFooterView {
    
    func set(title: String?) {
        self.titleLabel.text = title?.uppercased()
    }
    
}
