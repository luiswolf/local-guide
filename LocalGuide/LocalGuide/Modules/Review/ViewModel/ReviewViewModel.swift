//
//  ReviewViewModel.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import Foundation

class ReviewViewModel {
    
    static func reviews() -> [ReviewModel]? {
        guard let url = Bundle.main.url(forResource: "reviews", withExtension: "json") else {
            return nil
        }
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode([ReviewModel].self, from: data)
            let dynamicCount = Int.random(in: 0...jsonData.count)
            return Array(jsonData.shuffled().prefix(dynamicCount))
        } catch {
            print("error:\(error)")
        }
        return nil
    }
    
}
