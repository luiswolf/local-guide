//
//  LocationCollectionViewLayout.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import UIKit

class LocationCollectionViewLayout: UICollectionViewLayout {

    weak var delegate: LocationCollectionViewLayoutDelegate?
    
    private let numberOfColumns = 2
    private let cellPadding: CGFloat = 16
    
    private var cache: [UICollectionViewLayoutAttributes] = []
    private var contentHeight: CGFloat = 0
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else { return 0 }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }

    override class var layoutAttributesClass: AnyClass { LocationCollectionViewLayoutAttributes.self }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return cache.filter { $0.frame.intersects(rect) }
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
    override func prepare() {
        guard cache.isEmpty, let collectionView = collectionView else { return }
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        
        var xOffset: [CGFloat] = []
        for column in 0..<numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
        let numberOfItems = collectionView.numberOfItems(inSection: 0)
        for item in 0..<numberOfItems {
            let paddingVariant: (left: CGFloat, right: CGFloat) = {
                guard column == 0 else {
                    return (left: cellPadding * 0.5, right: cellPadding)
                }
                return (left: cellPadding, right: cellPadding * 0.5)
            }()
            let paddingBottom: CGFloat = {
                guard item == numberOfItems - 1 else { return 0 }
                return cellPadding
            }()
            
            let indexPath = IndexPath(item: item, section: 0)
            let width = columnWidth - cellPadding * 1.5
            
            let photoHeight = delegate?.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath, withWidth: width) ?? 180
            let annotationHeight = delegate?.collectionView(collectionView, heightForCaptionAtIndexPath: indexPath, withWidth: width) ?? 100
            
            let height = cellPadding + photoHeight + annotationHeight + cellPadding + paddingBottom
            let frame = CGRect(
                x: xOffset[column],
                y: yOffset[column],
                width: columnWidth,
                height: height
            )
            
            let insetFrame = frame.inset(by: UIEdgeInsets(
                top: cellPadding,
                left: paddingVariant.left,
                bottom: 0.0,
                right: paddingVariant.right
            ))
              
            let attributes: LocationCollectionViewLayoutAttributes = {
                let attributes = LocationCollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = insetFrame
                attributes.photoHeight = photoHeight
                return attributes
            }()
            
            cache.append(attributes)

            contentHeight = max(contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] + height
            
            column = column < (numberOfColumns - 1) ? (column + 1) : 0
        }
    }
    
}
