//
//  LocationCollectionViewController.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import UIKit
import Core

class LocationCollectionViewController: UICollectionViewController, LoaderProtocol, ErrorProtocol, ErrorViewDelegate {

    private typealias Constant = LocationConstant
    
    weak var coordinator: LocationCoordinatorProtocol?
    weak var errorDelegate: ErrorViewDelegate? { self }
    
    private var viewModel: LocationViewModelProtocol
    
    private lazy var dataProvider: LocationDataProvider = {
        let provider = LocationDataProvider(withCollectionView: collectionView)
        provider.delegate = self
        return provider
    }()
    
    init(withViewModel viewModel: LocationViewModelProtocol = LocationViewModel()) {
        self.viewModel = viewModel
        super.init(collectionViewLayout: LocationCollectionViewLayout())
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) { nil }
    
}

// MARK: - Lifecycle
extension LocationCollectionViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let layout = collectionView.collectionViewLayout as? LocationCollectionViewLayout {
            layout.delegate = dataProvider
        }
        
        collectionView.delegate = dataProvider
        collectionView.dataSource = dataProvider
        fetchData()
    }
    
}

// MARK: - LocationDataProviderDelegate
extension LocationCollectionViewController: LocationDataProviderDelegate {

    func didSelect(item: LocationModel) {
        coordinator?.detail(ofLocation: item)
    }

}

// MARK: - LocationViewModelDelegate
extension LocationCollectionViewController: LocationViewModelDelegate {
    
    func didGetData() {
        hideLoader()
        dataProvider.set(items: viewModel.getLocations())
        collectionView.reloadData()
    }
    
    func didGetError(withMessage message: String) {
        hideLoader()
        showError(withMessage: message)
    }
    
    func didTryAgain() {
        fetchData()
    }
    
}

// MARK: - Helper
extension LocationCollectionViewController {
    
    func fetchData() {
        hideError()
        showLoader()
        viewModel.fetch()
    }
    
}
