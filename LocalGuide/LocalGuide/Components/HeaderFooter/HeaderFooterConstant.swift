//
//  HeaderFooterConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

struct HeaderFooterConstant {
    
    enum Color {
        static let text = UIColor.topaz
    }
    
    enum Font {
        static let text = UIFont.header
    }
    
    enum Size {
        static let topMargin: CGFloat = 16.0
    }
    
}
