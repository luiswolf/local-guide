//
//  LocationAddressInformationData.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 24/03/21.
//

import UIKit

struct LocationAddressInformationData: InformationDataProtocol {
    
    private typealias Constant = LocationConstant
    
    var icon: UIImage? { Constant.Image.location }
    
    var description: String
    
    init(with address: String) {
        description = address
    }
    
}
