//
//  LocationCollectionViewLayoutAttributes.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 26/03/21.
//

import UIKit

class LocationCollectionViewLayoutAttributes: UICollectionViewLayoutAttributes {
  
    var photoHeight: CGFloat = 0.0
  
    override func copy(with zone: NSZone? = nil) -> Any {
        let copy = super.copy(with: zone) as! LocationCollectionViewLayoutAttributes
        copy.photoHeight = photoHeight
        return copy
    }
  
    override func isEqual(_ object: Any?) -> Bool {
        guard
            let attributes = object as? LocationCollectionViewLayoutAttributes,
            attributes.photoHeight == photoHeight
        else {
            return false
        }
        return super.isEqual(object)
    }
    
}
