//
//  LocationViewModel.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import Foundation
import Core

class LocationViewModel: NSObject, LocationViewModelProtocol {
    
    private typealias Constant = LocationConstant
    
    weak var delegate: LocationViewModelDelegate?
    
    private var locations: [LocationModel]?
    
    private let neworkingProvider: NetworkingProviderProtocol
    
    private lazy var request: LocationListRequest = {
        return LocationListRequest()
    }()
    
    required init(withProvider provider: NetworkingProviderProtocol = NetworkingProvider()) {
        self.neworkingProvider = provider
    }
    
    func getLocations() -> [LocationModel]? { locations }
    
}

// MARK: - Networking
extension LocationViewModel {
    
    func fetch() {
        neworkingProvider.perform(request) { [weak self] (response: LocationWrapperModel?) in
            
            guard let response = response else {
                self?.delegate?.didGetError(withMessage: Constant.Message.defaultError)
                return
            }
            
            guard let data = response.listLocations, !data.isEmpty else {
                self?.delegate?.didGetError(withMessage: Constant.Message.noDataError)
                return
            }

            self?.locations = data
            self?.delegate?.didGetData()
        }
    }
    
}
