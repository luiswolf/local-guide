//
//  AvatarConstant.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

struct AvatarConstant {
    
    enum Color {
        static func background() -> UIColor? {
            UIColor.Placheholder.avatar.randomElement()
        }
        static let border = UIColor.pinkish_grey
        static let text = UIColor.white
    }
    
    enum Font {
        static let text = UIFont.titleBig
    }
    
    enum Size {
        static let borderWidth: CGFloat = 1.0
        static let margin: CGFloat = 4.0
        static let height: CGFloat = 50.0
    }
    
}
