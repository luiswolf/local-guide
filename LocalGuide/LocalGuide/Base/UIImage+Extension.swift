//
//  UIImage+Extension.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 25/03/21.
//

import UIKit

extension UIImage {
    
    enum Placheholder {
        static let avatar: [UIImage?] = [
            UIImage(named: "avatar-1"),
            UIImage(named: "avatar-2"),
            UIImage(named: "avatar-3"),
            UIImage(named: "avatar-4")
        ]
        static let location: [UIImage?] = [
            UIImage(named: "restaurant-1"),
            UIImage(named: "restaurant-2"),
            UIImage(named: "restaurant-3"),
            UIImage(named: "restaurant-4"),
            UIImage(named: "restaurant-5"),
            UIImage(named: "restaurant-6"),
            UIImage(named: "restaurant-7"),
            UIImage(named: "restaurant-8"),
            UIImage(named: "restaurant-9"),
            UIImage(named: "restaurant-10"),
            UIImage(named: "restaurant-11")
        ]
    }
 
    class func colorForNavBar(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 0.5, height: 0.5)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
