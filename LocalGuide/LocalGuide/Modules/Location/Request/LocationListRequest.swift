//
//  LocationListRequest.swift
//  LocalGuide
//
//  Created by Luis Emilio Dias Wolf on 23/03/21.
//

import Foundation
import Core

struct LocationListRequest: NetworkingRequestProtocol {
    
    var method: NetworkingMethod { .get }
    
    var url: String {
        return LocationConstant.Endpoint.list
    }
    
}
